extends Node2D

signal solved

export var active_color := Color(0, 0.75, 0)

var _segments := Array()
var _solution_criteria := Array()

func _ready() -> void:
	var map := $TileMap
	var Pipe := preload('res://entities/Pipe.tscn')

	map.visible = false
	for cell in map.get_used_cells():
		var pipe := Pipe.instance()
		pipe.initialize(map, cell)
		self.add_child(pipe)
		self._segments.append(pipe)
# warning-ignore:return_value_discarded
		pipe.connect('rotated', self, '_update_flow')
	$Start.modulate = self.active_color
	self._segments.append($End)
	self._solution_criteria = self.get_tree().get_nodes_in_group('solution_criteria')


func _update_flow() -> void:
	for pipe in self._segments:
		pipe.active = false
	$Start.flow()
	for pipe in self._segments:
		if pipe.active:
			pipe.modulate = self.active_color
		else:
			pipe.modulate = pipe.INACTIVE_COLOR

	var solved := true
	for pipe in self._solution_criteria:
		if not pipe.active:
			solved = false
			break
	if solved:
		var background = $Background
		$Tween.interpolate_property(
			background,
			'modulate',
			background.modulate,
			Color(1, 1, 1, 0),
			2,
			Tween.TRANS_LINEAR
		)
		$AudioStreamPlayer.play()
		$Tween.start()
		yield($Tween, 'tween_all_completed')
		self.emit_signal('solved')
