extends Node2D


func speak(frame: int, text: String) -> void:
	$Portrait.frame = frame
	$Speech/Label.text = text
	$Speech.visible = true


func listen() -> void:
	$Portrait.frame = 0
	$Speech.visible = false
