extends Node2D

signal complete

var file := 'chapter1'
var listening := true

var _csv := File.new()
var _speakers : Array

func _ready():
	self._speakers = $Speakers.get_children()
# warning-ignore:return_value_discarded
	self._csv.open('res://assets/dialog/%s.csv' % self.file, self._csv.READ)
	self.next()


func next() -> void:
	if self._csv.eof_reached():
		self.listening = false
		self._csv.close()
		self.emit_signal('complete')
		return
	var row = self._csv.get_csv_line()
	for speaker in self._speakers:
		if speaker.name == row[0]:
			speaker.speak(int(row[1]), row[2])
		else:
			speaker.listen()
		if len(row) > 3 and row[3] != '':
			$AnimationPlayer.play(row[3])


func _input(event: InputEvent) -> void:
	if InputController.is_affirmative_input(self, event):
		self.next()
