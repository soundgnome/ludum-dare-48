extends CenterContainer

signal clear

var listening := false

func appear() -> void:
	var card = LevelController.get_chapter_card()
	$Label.get('custom_fonts/font').size = card['size']
	$Label.text = card['text']
	self.visible = true
	self.listening = true
	if card['autoclear']:
		$Timer.start()


func clear() -> void:
	if self.listening:
		self.listening = false
		self.emit_signal('clear')


func _input(event: InputEvent) -> void:
	if InputController.is_affirmative_input(self, event):
		self.clear()
