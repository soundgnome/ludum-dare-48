extends Node2D

onready var _jukebox := $MusicPlayer
onready var _tween := $Tween

func _ready():
	LevelController.game_window = self
	self.show_title_screen()


func show_chapter_card() -> void:
	var card: CenterContainer = $ChapterCard
	self._fade(1)
	yield(self._tween, 'tween_all_completed')
	$Level.visible = false
	$Dialog.visible = false
	card.appear()
	if not _jukebox.playing:
		self._jukebox.volume_db = 0
		self._jukebox.play()
	self._fade(0)
	yield(self._tween, 'tween_all_completed')


func show_dialog() -> void:
	var container : Node2D = $Dialog
	for child in container.get_children():
		child.queue_free()
	self._fade(1)
	yield(self._tween, 'tween_all_completed')
	$Level.visible = false
	$ChapterCard.visible = false
	container.visible = true
	var dialog = LevelController.get_dialog_instance()
	container.add_child(dialog)
	dialog.connect('complete', self, '_on_event_clear')
	self._fade(0)


func show_level() -> void:
	var container = $Level
	for child in container.get_children():
		child.queue_free()
	if self._jukebox.playing:
		$AnimationPlayer.play('silence')
	self._fade(1)
	yield(self._tween, 'tween_all_completed')
	$Dialog.visible = false
	$ChapterCard.visible = false
	container.visible = true
	var level = LevelController.get_level_instance()
	container.add_child(level)
	level.connect('solved', self, '_on_event_clear')
	self._fade(0)


func show_title_screen() -> void:
	var container := $Level
	container.visible = false
	$ChapterCard.visible = false
	for child in container.get_children():
		child.queue_free()
	if self._jukebox.playing:
		$AnimationPlayer.play('silence')
	var instance : Node2D = ResourceLoader.load('res://ui/TitleScreen.tscn').instance()
	container.add_child(instance)
# warning-ignore:return_value_discarded
	instance.connect('clear', self, '_on_event_clear')
	container.visible = true


func _fade(alpha : float) -> void:
	var curtain = $Curtain
	if alpha == 1:
		curtain.visible = true
	self._tween.interpolate_property(
		curtain,
		'modulate',
		curtain.modulate,
		Color(1, 1, 1, alpha),
		1,
		Tween.TRANS_LINEAR
	)
	self._tween.start()
	if alpha == 0:
		yield(self._tween, 'tween_all_completed')
		curtain.visible = false


func _on_event_clear() -> void:
	var event : int = LevelController.get_next_event()
	if event == LevelController.EventType.CHAPTER_CARD:
		self.show_chapter_card()
	elif event == LevelController.EventType.DIALOG:
		self.show_dialog()
	elif event == LevelController.EventType.PUZZLE:
		self.show_level()
	elif event == LevelController.EventType.TITLE_SCREEN:
		self.show_title_screen()
