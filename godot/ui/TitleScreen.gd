extends Node2D

signal clear

onready var listening := true


func clear() -> void:
	if self.listening:
		self.listening = false
		self.emit_signal('clear')


func _input(event: InputEvent) -> void:
	if InputController.is_affirmative_input(self, event):
		self.clear()
