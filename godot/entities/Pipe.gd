extends Node2D

signal rotated

const INACTIVE_COLOR = Color(0.6, 0.6, 0.6)
const ROTATIONS = {
	0: 0,
	1: 270,
	2: 90,
	3: 180,
	5: 90,
	6: 270,
}

export var active := false

var _clicked := false
var _rotating := false
var _sprite : Sprite

onready var _tween := $Tween

func initialize(map: TileMap, cell: Vector2) -> void:
	self._sprite = $Sprite
	self.scale = map.scale
	self.position = (map.map_to_world(cell) * map.scale) + map.position
	self._sprite.frame = map.get_cellv(cell)

	var x = int(cell.x)
	var y = int(cell.y)
	self._sprite.rotation_degrees = self.ROTATIONS[ \
		int(map.is_cell_x_flipped(x, y)) \
		+ (int(map.is_cell_y_flipped(x, y)) << 1) \
		+ (int(map.is_cell_transposed(x, y)) << 2) \
	]


func flow() -> void:
	for area in $Sprite/Area2D.get_overlapping_areas():
		if not area.owner.active:
			area.owner.active = true
			area.owner.flow()


func _ready() -> void:
	self._sprite = $Sprite
	if not self.active:
		self.modulate = self.INACTIVE_COLOR
	if self._sprite.frame == 0:
		$TextureButton.mouse_filter = Control.MOUSE_FILTER_IGNORE
		self.add_to_group('solution_criteria')
		return

	$Sprite/Area2D/Top.queue_free()
	if self._sprite.frame == 2:
		$Sprite/Area2D/Bottom.queue_free()
	elif self._sprite.frame == 3:
		$Sprite/Area2D/Right.queue_free()


func _on_TextureButton_pressed() -> void:
	if self._sprite.frame == 0:
		return
	if self._rotating:
		self._clicked = true
		return
	self._rotating = true
	self._tween.interpolate_property(
		self._sprite,
		'rotation_degrees',
		self._sprite.rotation_degrees,
		self._sprite.rotation_degrees + 90,
		0.2,
		Tween.TRANS_SINE,
		Tween.EASE_IN_OUT
	)
	$AudioStreamPlayer.play()
	self._tween.start()
	yield(self._tween, 'tween_all_completed')
	if self._sprite.rotation_degrees > 359:
		self._sprite.rotation_degrees = 0
	self._rotating = false
	self.emit_signal('rotated')
	if self._clicked:
		self._clicked = false
		self._on_TextureButton_pressed()
