extends Node


func is_affirmative_input(node: CanvasItem, event: InputEvent) -> bool:
	if not node.listening:
		return false
	if event.is_class('InputEventKey') or event.is_class('InputEventMouseButton'):
		return not event.pressed
	return false
