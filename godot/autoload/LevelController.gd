extends Node

enum EventType { TITLE_SCREEN, CHAPTER_CARD, DIALOG, PUZZLE }

const EVENTS =[
	[EventType.TITLE_SCREEN],
	[EventType.CHAPTER_CARD, 'chapter1'],
	[EventType.DIALOG, 'chapter1'],
	[EventType.PUZZLE, 'Pipe1'],
	[EventType.CHAPTER_CARD, 'chapter2'],
	[EventType.DIALOG, 'chapter2'],
	[EventType.PUZZLE, 'Pipe2'],
	[EventType.CHAPTER_CARD, 'chapter3'],
	[EventType.DIALOG, 'chapter3'],
	[EventType.PUZZLE, 'Pipe3'],
	[EventType.CHAPTER_CARD, 'chapter4'],
	[EventType.DIALOG, 'chapter4'],
	[EventType.PUZZLE, 'Pipe4'],
	[EventType.DIALOG, 'ending'],
	[EventType.CHAPTER_CARD, 'victory'],
	[EventType.CHAPTER_CARD, 'credits'],
]

var game_window : Node2D

var _current_event : int = 0
var _chapter_text := Dictionary()


func _ready() -> void:
	var csv := File.new()
# warning-ignore:return_value_discarded
	csv.open('res://assets/chapter_cards.csv', csv.READ)
	while not csv.eof_reached():
		var row := csv.get_csv_line()
		self._chapter_text[row[0]] = {
			'size': int(row[1]),
			'autoclear': bool(row[2]),
			'text': row[3],
		}
	csv.close()


func get_chapter_card() -> Dictionary:
	return self._chapter_text[self.EVENTS[self._current_event][1]]


func get_dialog_instance() -> Node2D:
	var instance = ResourceLoader.load('res://dialog/Dialog.tscn').instance()
	instance.file = self.EVENTS[self._current_event][1]
	return instance


func get_level_instance() -> Node2D:
	return ResourceLoader.load('res://levels/%s.tscn' % self.EVENTS[self._current_event][1]).instance()


func get_next_event() -> int:
	self._current_event += 1
	if self._current_event >= len(self.EVENTS):
		self._current_event = 0
	return self.EVENTS[self._current_event][0]


func start_game() -> void:
	self._current_event = 0
	self.game_window.show_chapter_card()
